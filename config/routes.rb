Rails.application.routes.draw do
  get '/login', to: 'session#login'
  post '/sign_up', to: 'register#sign_up'
  get '/current_user', to: 'application#user_must_exist'

  resources :friend_requests
  resources :likes
  resources :comments
  resources :posts
  resources :users, except: [:create]
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
