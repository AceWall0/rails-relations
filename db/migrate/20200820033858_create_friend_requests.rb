class CreateFriendRequests < ActiveRecord::Migration[5.2]
  def change
    create_table :friend_requests do |t|
      t.integer :sender
      t.integer :receiver
      t.boolean :accepted, default: false

      t.timestamps
    end
  end
end
