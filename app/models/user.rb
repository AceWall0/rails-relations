class User < ApplicationRecord
    has_secure_password 
    has_many :friend_request
    has_many :likes
    has_many :posts
    has_many :comments
    validates :name, :email, :birth_date, presence: true
    validates :email, uniqueness: true, format: { with: URI::MailTo::EMAIL_REGEXP }
end
