class JsonWebToken
    secret = ENV["JSON_SECRET_KEY"]

    def self.encode(payload)
        JWT.encode(payload, secret)
    end
    
    def self.decode(token)
        begin
            decoded = JWT.decode(token, secret)
        rescue => exception
            return nill
        end
    end
    
end