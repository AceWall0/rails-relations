class FriendRequest < ApplicationRecord
    validates :sender, :receiver, presence :true
    has_one :sender, foreign_key: :user_id
    has_one :receiver, foreign_key: :user_id
end
