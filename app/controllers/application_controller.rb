class ApplicationController < ActionController::API
    def current_user
        header = request.headers["Authorization"]
        return nill unless header.present?
        @decoded = JsonWebToken.decode(header)
        return nill unless @decoded
        user = User.find_by(id: @decoded[0]["user_id"])
    end

    def user_must_exist
        if current_user.present?
            render json: {message: "Logged in!"}
        end
    end
    
end
